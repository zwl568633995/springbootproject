package cn.sigo.live.api.interceptor;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.*;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.*;
import cn.hutool.crypto.*;

import java.io.IOException;

/**
 * 权限过滤 拦截器
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

    private String token = "85a6100221e844079e086e502680a9c9";
    private static final String SIGO_KEY = "572026ADF0F945759BD1D37F90E56776";

    /**
     * 处理权限过滤
     *
     * @auther: wangcong
     * @date: 2018/12/11 20:10
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;

       /* // 获取Header
        String signature = request.getHeader("signature");
        String noncestr = request.getHeader("noncestr");
        String timestamp = request.getHeader("timestamp");

        if (StrUtil.isNotEmpty(signature) && SIGO_KEY.equals(signature) && StrUtil.isNotEmpty(timestamp) && SIGO_KEY.equals(timestamp)) {
            return true;
        } else {
            if (StrUtil.isNotEmpty(signature) && StrUtil.isNotEmpty(noncestr) && StrUtil.isNotEmpty(timestamp)) {
                //判断时间戳
                if (timestamp.length() == 10 && IsTime(Convert.toInt(timestamp), 5)) {
                    //判断签名
                    if (!signature.toLowerCase().equals(GetMd5Signature(noncestr, timestamp).toLowerCase())) {
                        SetReponse(HttpServletResponse.SC_BAD_REQUEST, "签名错误", response);
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    SetReponse(HttpServletResponse.SC_BAD_REQUEST, "时间戳无效", response);
                    return false;
                }
            } else {
                SetReponse(HttpServletResponse.SC_BAD_REQUEST, "参数缺失：signature、noncestr、timestamp，缺一不可", response);
                return false;
            }
        }*/
    }

    /**
     * 功能描述:生成Md5验签
     *
     * @param: noncestr 随即字符串，timestamp 时间戳
     * @return: md5加密串
     * @auther: wangcong
     * @date: 2018/12/12 13:48
     */
    private String getMd5Signature(String noncestr, String timestamp) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("noncestr=" + noncestr + "&");
        strBuilder.append("timestamp=" + timestamp + "&");
        strBuilder.append("token=" + token);
        return SecureUtil.md5().digestHex(strBuilder.toString());
    }

    /**
     * 功能描述:判断是否是时间
     *
     * @param: timestamp 10位时间戳 minutes 差异分钟
     * @return: 是否在有效时间内
     * @auther: wangcong
     * @date: 2018/12/12 13:23
     */
    private boolean isTime(long timestamp, int minutes) {
        DateTime dateTime = DateUtil.date(timestamp * 1000);
        DateTime beginTime = DateUtil.offsetMinute(DateTime.now(), (minutes * -1));
        DateTime endTime = DateUtil.offsetMinute(DateTime.now(), minutes);
        return DateUtil.isIn(dateTime, beginTime, endTime);
    }

    /**
     * 功能描述:自定义输出
     *
     * @auther: wangcong
     * @date: 2018/12/11 23:15
     */
    private void setReponse(int statusCode, String message, HttpServletResponse response) throws IOException {
        response.setStatus(statusCode);
        response.setCharacterEncoding("UTF-8");
        //这句话是解决乱码的
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        response.getWriter().append(message);
    }
}
