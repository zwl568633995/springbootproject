package cn.sigo.live.api.model.response;

import lombok.Data;

/**
 * 标签管理
 */
@Data
public class TagResponse extends TagCountResponse{
    /**
     * 标签顺序
     */
    private Integer sort;
    /**
     * 标签下评论数目
     */
    private Integer commentCount;
}
