package cn.sigo.live.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class TagPostByTopDesc {
    private Integer postId;
    private Boolean isTop;
}
