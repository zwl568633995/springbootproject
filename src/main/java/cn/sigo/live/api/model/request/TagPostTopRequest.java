package cn.sigo.live.api.model.request;

import lombok.Data;

/**
 * 标签帖子请求实体
 */
@Data
public class TagPostTopRequest extends PageRequest {
    /**
     * 标签ID
     */
    private Integer tagId;
    /**
     * 帖子title
     */
    private String postTitle;
    /**
     * 发布帖子的用户名
     */
    private String publishUserName;
    /**
     * 发布开始时间
     */
    private String pubbegintime;
    /**
     * 发布结束时间
     */
    private String pubendtime;
    /**
     * 是否精华
     */
    private String isbest;
    /**
     * 状态
     */
    private String delState;
    /**
     * 是否允许评论
     */
    private String isallowcomment;
    /**
     * 排序字段
     */
    private String sortname;
    /**
     * 是否倒序
     */
    private String isdesc;
}
