package cn.sigo.live.api.model.response;

import lombok.Data;

import java.util.List;

/**
 *  分页数据
 */
@Data
public class PageResponse<T,K> {
    /**
     * 总行数
     */
    private long totalCount;
    /**
     * 当前页
     */
    private int pageNo;
    /**
     * 每页显示
     */
    private int pageSize;
    /**
     * 总页数
     */
    private int totalPages;

    /**
     * 分页数据
     */
    private List<T> data;

    /**
     * 附加信息
     */
    private K info;
}
