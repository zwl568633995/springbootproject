package cn.sigo.live.api.model.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 *  base数据
 */
@Data
public class CommonResponse<T> {
    /**
     *  数据集合
     */
    private List<T> data;
    /**
     *  是否处理成功
     */
    private boolean isSuccess;

    public CommonResponse() {
        this.data=new ArrayList<T>();
        this.isSuccess=false;
    }
}