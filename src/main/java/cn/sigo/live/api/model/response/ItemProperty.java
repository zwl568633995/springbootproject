package cn.sigo.live.api.model.response;

import lombok.Data;

/**
 * 商品数据
 */
@Data
public class ItemProperty {
    /**
     * 商品id
     */
    private Integer itemId;
    /**
     * 商品名称
     */
    private String itemName;
    /**
     * 图片地址
     */
    private String imageUrl;
}
