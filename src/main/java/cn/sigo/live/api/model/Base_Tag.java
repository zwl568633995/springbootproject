package cn.sigo.live.api.model;

import lombok.Data;

/**
 *  标签管理
 */
@Data
public class Base_Tag {
    /**
     *  主键
     */
    private Integer id;

    /**
     *  标签名称
     */
    private String name;

    /**
     *  排序
     */
    private Integer sort;

    /**
     *  标签状态
     */
    private String state;

    /**
     *  是否删除
     */
    private Boolean isDel;
}