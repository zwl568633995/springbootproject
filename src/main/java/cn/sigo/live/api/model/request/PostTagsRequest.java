package cn.sigo.live.api.model.request;

import lombok.Data;

import java.util.List;

/**
 * 标签下的帖子置顶
 */
@Data
public class PostTagsRequest {
    /**
     * 帖子集合
     */
    private List<Integer> postIds;

    /**
     * 标签ID
     */
    private Integer tagId;

    /**
     * 是否置顶
     */
    private Boolean isTop;
}
