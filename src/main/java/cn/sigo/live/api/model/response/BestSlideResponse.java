package cn.sigo.live.api.model.response;

import cn.sigo.live.api.model.Best_Slide;
import lombok.Data;

@Data
public class BestSlideResponse extends Best_Slide {
     private String title;
     private String cover;
     private String heading;
     private String addtimestr;
}
