package cn.sigo.live.api.model;

public class ElasticSearchValue {
    public String getEsIdValue() {
        return esIdValue;
    }

    public void setEsIdValue(String esIdValue) {
        this.esIdValue = esIdValue;
    }

    private String esIdValue;
}
