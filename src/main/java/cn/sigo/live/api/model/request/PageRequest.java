package cn.sigo.live.api.model.request;

import lombok.Data;

/**
 *  分页请求
 */
@Data
public class PageRequest {
    /**
     *  页码
     */
    private Integer pagenum;
    /**
     *  每页数量
     */
    private Integer pagesize;
}
