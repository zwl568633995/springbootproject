package cn.sigo.live.api.model.request;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 *  帖子请求
 */
@Data
public class PostRequest extends PageRequest {

    public PostRequest(){
        this.titiledef="";
        this.numberorphone="";
        this.itemdef="";
        this.pubbegintime="";
        this.pubendtime="";
        this.visitcountmin="";
        this.visitcountmax="";
        this.commentcountmin="";
        this.commentcountmax="";
        this.isBest="";
        this.state="";
        this.tagname="";
        this.idList=null;
    }

    /**
     * 主题
     */
    private String  titiledef;
    /**
     * 账号或手机号
     */
    private String numberorphone;
    /**
     * 相关商品
     */
    private String itemdef;
    /**
     * 发表起始日期
     */
    private String pubbegintime;
    /**
     * 发表结束日期
     */
    private String pubendtime;
    /**
     * 查看最小次数
     */
    private String visitcountmin;
    /**
     * 查看最大次数
     */
    private String visitcountmax;
    /**
     * 最小评论次数
     */
    private String commentcountmin;
    /**
     * 最大评论次数
     */
    private String commentcountmax;
    /**
     * 是否精华帖
     */
    private String isBest;
    /**
     * 是否被删除
     */
    private String state;

    /**
     * 标签名称
     */
    private String tagname;

    /**
     * 帖子id的集合
     */
    private List<Integer> idList;
    /**
     * 是否允许评论
     */
    private String isallowcomment;
    /**
     * 排序字段
     */
    private String sortname;
    /**
     * 是否倒序
     */
    private String isdesc;
}
