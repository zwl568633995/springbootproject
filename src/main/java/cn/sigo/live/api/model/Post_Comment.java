package cn.sigo.live.api.model;

import lombok.Data;

import java.util.Date;

/**
 *  帖子评论
 */
@Data
public class Post_Comment {
    /**
     *  主键
     */
    private Integer id;

    /**
     *  帖子id
     */
    private Integer postId;

    /**
     *  内容
     */
    private String content;

    /**
     *  发布时间
     */
    private Date publishTime;

    /**
     *  状态
     */
    private String state;

    /**
     *  点赞数
     */
    private Integer countLike;
}