package cn.sigo.live.api.model;

import lombok.Data;

import java.util.Date;

@Data
public class Best_Slide {
    /**
     *  主键
     */
    private Integer id;
    /**
     *  轮播位类型（post=帖子、img=图片）
     */
    private String type;
    /**
     *  帖子ID
     */
    private Integer postid;
    /**
     *  图片链接，https地址
     */
    private String image;
    /**
     *  点击图片跳转链接
     */
    private String url;
    /**
     *  状态（enabled=启用、disabled=停用）
     */
    private String state;
    /**
     *  排序值
     */
    private Integer sort;
    /**
     *  添加时间
     */
    private Date addtime;
}