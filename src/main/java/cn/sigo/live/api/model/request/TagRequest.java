package cn.sigo.live.api.model.request;

import lombok.Data;

import java.util.List;

/**
 * 标签请求实体
 */
@Data
public class TagRequest {
    /**
     * 标签id
     */
    private Integer tagId;
    /**
     * 名称
     */
    private String name;
    /**
     * 更新的标签id
     */
    private List<Integer> updateTagIds;
}
