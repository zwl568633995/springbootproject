package cn.sigo.live.api.model.response;

import lombok.Data;

/**
 * 标签管理:获取标签下帖子数量
 */
@Data
public class TagCountResponse {
    /**
     * 标签主键
     */
    private Integer id;
    /**
     * 标签名称
     */
    private String name;
    /**
     * 标签下帖子数目
     */
    private Integer postCount;
}
