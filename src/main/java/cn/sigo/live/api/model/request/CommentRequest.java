package cn.sigo.live.api.model.request;

import lombok.Data;
/**
 * 评论请求实体
 */
@Data
public class CommentRequest extends PageRequest {
    /**
     * 评论内容
     */
    private String  comment;
    /**
     * 账号或手机号
     */
    private String numberorphone;
    /**
     * 帖子主题
     */
    private String titile;
    /**
     * 发表起始日期
     */
    private String commentbegintime;
    /**
     * 发表结束日期
     */
    private String commentendtime;

    /**
     * 最小评论点赞数
     */
    private String likecountmin;
    /**
     * 最大评论点赞数
     */
    private String likecountmax;
    /**
     * 帖子id
     */
    private String postid;
}
