package cn.sigo.live.api.model.request;

import lombok.Data;

import java.util.List;

/**
 *  通用集合请求
 */
@Data
public class BatchRequest<T> {
    /**
     * 请求的字段集合
     */
    List<T> collection;
}
