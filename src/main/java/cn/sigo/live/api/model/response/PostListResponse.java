package cn.sigo.live.api.model.response;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 *  帖子列表数据
 */
@Data
public class PostListResponse {
    /**
     * 用户昵称
     */
    private String publishNickname;
    /**
     * 封面
     */
    private String cover;
    /**
     * 标题
     */
    private String title;
    /**
     * 发帖时间
     */
    private String publishTime;
    /**
     * 查看数
     */
    private Integer countVisit;
    /**
     * 评论数
     */
    private Integer countComment;
    /**
     * 商品数
     */
    private Integer countItems;
    /**
     * 是否允许评论
     */
    private String isAllowComment;
    /**
     * 是否精华
     */
    private String isBest;
    /**
     * 状态
     */
    private String state;
    /**
     * 头像
     */
    private String photo;
    /**
     * 帖子ID
     */
    private Integer postId;
    /**
     * 是否置顶
     */
    private Integer isTop;
    /**
     * 标签id
     */
    private Integer tagId;
    /**
     * 帖子下面的商品以及名称
     */
    private List<ItemProperty> itemsAndImage;
}
