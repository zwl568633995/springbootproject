package cn.sigo.live.api.model;

import lombok.Data;

import java.util.Date;

/**
 *  帖子管理
 */
@Data
public class Post {
    /**
     *  主键
     */
    private Integer id;

    /**
     *  标题
     */
    private String title;

    /**
     *  封面
     */
    private String cover;

    /**
     *  题头（图片或短视频）
     */
    private String heading;

    /**
     *  题头类型(img、video)
     */
    private String headingType;

    /**
     *  发布时间
     */
    private Date publishTime;

    /**
     * 发布用户id
     */
    private Long publishUserid;

    /**
     *  发布用户名
     */
    private String publishUsername;

    /**
     *  帖子状态
     */
    private String state;

    /**
     *  是否精华
     */
    private Boolean isBest;

    /**
     *  查看数
     */
    private Integer countVisit;

    /**
     * 评论数
     */
    private Integer countComment;

    /**
     * 点赞数
     */
    private Integer countLike;

    /**
     * 被收藏次数
     */
    private Integer countFavourite;

    /**
     * 被分享次数
     */
    private Integer countShare;

    /**
     * 是否允许评论
     */
    private Boolean isAllowComment;

    /**
     * 内容
     */
    private String content;
}