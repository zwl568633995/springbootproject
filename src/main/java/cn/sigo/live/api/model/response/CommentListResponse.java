package cn.sigo.live.api.model.response;

import lombok.Data;

/**
 *  评论数据集合
 */
@Data
public class CommentListResponse {
    /**
     * 评论id
     */
    private Integer commentId;
    /**
     * 评论时间
     */
    private String commentTime;
    /**
     * 评论者
     */
    private String commentor;
    /**
     * 评论内容
     */
    private String comment;
    /**
     * 点赞数
     */
    private String likeCount;
    /**
     * 被评论的帖子
     */
    private String title;
    /**
     * 是否精华帖
     */
    private boolean isBest;
    /**
     * 帖子的状态
     */
    private String state;
    /**
     * 评论者的昵称
     */
    private String nickName;
    /**
     * 头像
     */
    private String photo;
}
