package cn.sigo.live.api.util;

import cn.sigo.live.api.model.ElasticSearchValue;
import com.alibaba.fastjson.JSON;
import org.springframework.amqp.core.AmqpTemplate;

import java.util.List;

/**
 * 推送mq更新es
 */
public class ESUpdateHelper {
    public static void updateByPostId(List<Integer> postids, AmqpTemplate rabbitTemplate){
        for (Integer item : postids) {
            ElasticSearchValue es = new ElasticSearchValue();
            es.setEsIdValue(item.toString());
            rabbitTemplate.convertAndSend("exchange.sigo.b2c.elasticsearch", "index_live_post", JSON.toJSONString(es));
        }
    }
}
