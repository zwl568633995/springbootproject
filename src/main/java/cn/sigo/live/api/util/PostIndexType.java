package cn.sigo.live.api.util;

/**
 *  es评论
 */
public class PostIndexType {
    /**
     *  Index
     */
    public static final String INDEX_NAME = "index_live_post";

    /**
     *  Type
     */
    public static final String TYPE = "_doc";
}
