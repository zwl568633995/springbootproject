package cn.sigo.live.api.util;

/**
 * 帖子状态枚举值 admindeleted 后台删除，published发布状态
 * @Author: zhengweilin
 * @Date: 2019-01-08 9:59
 */
public enum PostState {
    admindeleted,
    published
}
