package cn.sigo.live.api.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

/**
 * 功能描述: Redis操作方法
 *
 * @author zhengweilin
 * @date 2019/01/03 15:48
 */
@Component
public class RedisHelper {
    public static RedisHelper redisHelper;

    @Autowired
    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init() {
        redisHelper = new RedisHelper();
    }

    /**
     * 读取缓存
     *
     * @param dbName 库名
     * @param key    键值
     * @return String类型值
     */
    public String get(String dbName, String key) {
        String result = null;

        setRedisTemplate(dbName);
        ValueOperations<Serializable, String> operations = redisTemplate.opsForValue();
        result = operations.get(key);
        return result;
    }

    /**
     * 功能描述: 设置Redis连接属性
     *
     * @author wangcong
     * @date 2018/12/28 17:09
     */
    private void setRedisTemplate(String dbName) {
        boolean isRestConnection = false;
        int dbIndex = getDBIndex(dbName);
        LettuceConnectionFactory redisConnectionFactory = (LettuceConnectionFactory) redisTemplate.getConnectionFactory();

        if (redisConnectionFactory.getDatabase() != dbIndex) {
            redisConnectionFactory.setDatabase(dbIndex);
            isRestConnection = true;
        }

        redisTemplate.setConnectionFactory(redisConnectionFactory);
        if (isRestConnection == true) {
            redisConnectionFactory.resetConnection();
        }

        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setValueSerializer(stringRedisSerializer);
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.setHashValueSerializer(stringRedisSerializer);
        redisTemplate.afterPropertiesSet();
    }

    /**
     * 功能描述: 获取DB对应下标
     *
     * @param dbName 库名
     * @return 参数
     * @auther wangcong
     * @date 2018/12/28 15:32
     */
    private int getDBIndex(String dbName) {
        int dbIndex = -1;
        switch (dbName.toLowerCase()) {
            case "temp_cache":
                dbIndex = 0;
                break;
            case "b2c_item":
                dbIndex = 1;
                break;
            case "b2c_item_sku":
                dbIndex = 2;
                break;
            case "b2c_item_property_sku":
                dbIndex = 3;
                break;
            case "b2c_item_category":
                dbIndex = 4;
                break;
            case "b2c_promotion_itemdiscount":
                dbIndex = 5;
                break;
            case "b2c_promotion_itembuygive":
                dbIndex = 6;
                break;
            case "b2c_promotion_itemsuit":
                dbIndex = 7;
                break;
            case "b2c_promotion_itemraisetobuy":
                dbIndex = 8;
                break;
            case "b2c_promotion_orderreduct":
            case "b2c_item_sku_stock":
                dbIndex = 9;
                break;
            case "b2c_ordercode":
                dbIndex = 10;
                break;
            case "b2c_promotion_rule":
                dbIndex = 11;
                break;
            case "b2c_item_brand":
                dbIndex = 12;
                break;
            case "b2c_region_province":
                dbIndex = 13;
                break;
            case "b2c_region":
                dbIndex = 14;
                break;
            case "b2c_others":
                dbIndex = 15;
                break;
            case "b2c_cart":
                dbIndex = 16;
                break;
            case "b2c_cart_relation_to_user":
                dbIndex = 17;
                break;
            case "b2c_promotion_cartreduct":
                dbIndex = 18;
                break;
            case "b2c_promotion_orderreduct_new":
                dbIndex = 19;
                break;
            case "b2c_promotion_freepostage":
                dbIndex = 20;
                break;
            default:
                break;
        }
        return dbIndex;
    }
}
