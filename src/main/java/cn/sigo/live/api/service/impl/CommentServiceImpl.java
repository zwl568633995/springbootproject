package cn.sigo.live.api.service.impl;

import cn.sigo.live.api.filter.Http400Exception;
import cn.sigo.live.api.mapper.Post_CommentMapper;
import cn.sigo.live.api.model.request.CommentRequest;
import cn.sigo.live.api.model.response.CommentListResponse;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.PageResponse;
import cn.sigo.live.api.model.response.PostListResponse;
import cn.sigo.live.api.service.CommentService;
import cn.sigo.live.api.util.PostIndexType;
import cn.sigo.live.api.util.PostState;
import cn.sigo.live.api.util.RedisHelper;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述:评论管理
 *
 * @author: zhengweilin
 * @date: 2018/12/18
 */
@Service("CommentService")
public class CommentServiceImpl implements CommentService {
    @Autowired
    private Post_CommentMapper commentMapper;
    @Autowired
    private JestClient jestClient;

    /**
     * 查询列表
     */
    @Override
    public PageResponse<CommentListResponse, Null> query(CommentRequest commentRequest) {
        if(commentRequest.getPagenum()==null||commentRequest.getPagesize()==null){
            throw new Http400Exception("paramerror","参数错误");
        }

        Map<String, Object> param = new HashMap<>();
        PageResponse<CommentListResponse, Null> response = new PageResponse<CommentListResponse, Null>();
        ////es查询userid
        if (!"".equals(commentRequest.getNumberorphone())) {
            getUserIdByEs(commentRequest.getNumberorphone(), param);
        }
        if (commentRequest.getCommentbegintime() != null && !"".equals(commentRequest.getCommentbegintime())) {
            commentRequest.setCommentbegintime(commentRequest.getCommentbegintime() + " 00:00:01");
        }
        if (commentRequest.getCommentendtime() != null && !"".equals(commentRequest.getCommentendtime())) {
            commentRequest.setCommentendtime(commentRequest.getCommentendtime() + " 23:59:59");
        }

        ////查询条件封装
        param.put("comment", commentRequest.getComment());
        param.put("titile", commentRequest.getTitile());
        param.put("pubBeginTime", commentRequest.getCommentbegintime());
        param.put("pubEndTime", commentRequest.getCommentendtime());
        param.put("likeCountMin", commentRequest.getLikecountmin());
        param.put("likeCountMax", commentRequest.getLikecountmax());
        param.put("postid", commentRequest.getPostid());

        PageHelper.startPage(commentRequest.getPagenum() + 1, commentRequest.getPagesize());
        List<CommentListResponse> list = commentMapper.query(param);
        if (param.get("nickName") != null && param.get("nickName") != "") {
            for (CommentListResponse coment : list) {
                coment.setNickName(param.get("nickName").toString());
                coment.setPhoto(param.get("profile_photo").toString());
                if (coment.isBest()) {
                    coment.setTitle("【精华】" + coment.getTitle());
                }
            }
        } else {
            List<String> userids = new ArrayList<String>();
            for (CommentListResponse coment : list) {
                userids.add(coment.getCommentor());
                if (coment.isBest()) {
                    coment.setTitle("【精华】" + coment.getTitle());
                }
            }

            ////ES查询NickName
            getNickNameByEs(userids, list);
        }

        PageInfo pageInfo = new PageInfo(list);
        response.setData(list);
        response.setTotalCount(pageInfo.getTotal());

        return response;
    }

    /**
     * 修改评论状态
     */
    @Override
    public CommonResponse<org.apache.ibatis.jdbc.Null> changeState(List<Integer> postids, boolean isEnabled) {
        if(postids==null){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<org.apache.ibatis.jdbc.Null> response = new CommonResponse<org.apache.ibatis.jdbc.Null>();
        Map<String, Object> map = new HashMap<>();
        map.put("stateName", isEnabled ? PostState.published : PostState.admindeleted);
        map.put("idList", postids);
        int i = commentMapper.batchUpdateByPrimaryKey(map);
        response.setSuccess(i>0?true:false);

        return response;
    }

    /**
     * Es获取NickName
     */
    private void getNickNameByEs(List<String> userids, List<CommentListResponse> list) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        QueryBuilder queryBuilderPhone = QueryBuilders.nestedQuery("publish_user", QueryBuilders.boolQuery().must(QueryBuilders.termsQuery("publish_user.userid", userids)), ScoreMode.None);

        boolQueryBuilder.must(queryBuilderPhone);
        ////查询参数
        QueryBuilder queryBuilder = boolQueryBuilder;
        searchSourceBuilder.query(queryBuilder);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex(PostIndexType.INDEX_NAME).addType(PostIndexType.TYPE).build();
        try {
            ////查询结果
            JestResult result = jestClient.execute(search);
            if (!result.isSucceeded()) {
                return;
            }

            List<SearchResult.Hit<JSONObject, Void>> hits = ((SearchResult) result).getHits(JSONObject.class);
            PageResponse<PostListResponse, Null> postPages = new PageResponse<PostListResponse, Null>();
            postPages.setTotalCount(hits.size());
            List<PostListResponse> listPost = new ArrayList<>();
            for (SearchResult.Hit<JSONObject, Void> hit : hits) {
                PostListResponse postResponse = new PostListResponse();
                JSONObject post = hit.source;
                JSONObject publishUser = post.getJSONObject("publish_user");
                Integer userid = publishUser == null ? 0 : publishUser.getInteger("userid");
                String nickName = publishUser == null ? "" : publishUser.getString("nickname");
                String profilePhoto = publishUser == null ? "" : publishUser.getString("profile_photo");
                for (CommentListResponse coment : list) {
                    if (coment.getCommentor().equals(userid.toString())) {
                        coment.setNickName(nickName);
                        coment.setPhoto(profilePhoto);
                    }
                }
            }
        } catch (IOException e) {
            throw new Http400Exception("IOException",e.getMessage());
        }
    }

    /**
     * Es获取UserId
     */
    private void getUserIdByEs(String numberOrPhone, Map<String, Object> param) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        QueryBuilder queryBuilderPhone = QueryBuilders.nestedQuery("publish_user", QueryBuilders.boolQuery().must(QueryBuilders.termQuery("publish_user.phone", "" + numberOrPhone + "")), ScoreMode.None);
        QueryBuilder queryBuilderUserName = QueryBuilders.nestedQuery("publish_user", QueryBuilders.boolQuery().must(QueryBuilders.termQuery("publish_user.username", "" + numberOrPhone + "")), ScoreMode.None);
        boolQueryBuilder.should(queryBuilderPhone);
        boolQueryBuilder.should(queryBuilderUserName);
        ////查询参数
        QueryBuilder queryBuilder = boolQueryBuilder;
        searchSourceBuilder.query(queryBuilder);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex(PostIndexType.INDEX_NAME).addType(PostIndexType.TYPE).build();
        try {
            ////查询结果
            JestResult result = jestClient.execute(search);
            if (!result.isSucceeded()) {
                return;
            }

            List<SearchResult.Hit<JSONObject, Void>> hits = ((SearchResult) result).getHits(JSONObject.class);
            PageResponse<PostListResponse, Null> postPages = new PageResponse<PostListResponse, Null>();
            postPages.setTotalCount(hits.size());
            List<PostListResponse> listPost = new ArrayList<>();
            for (SearchResult.Hit<JSONObject, Void> hit : hits) {
                PostListResponse postResponse = new PostListResponse();
                JSONObject post = hit.source;
                JSONObject publishUser = post.getJSONObject("publish_user");
                Integer userid = publishUser == null ? 0 : publishUser.getInteger("userid");
                String nickName = publishUser == null ? "" : publishUser.getString("nickname");
                String profilePhoto = publishUser == null ? "" : publishUser.getString("profile_photo");
                param.put("numberOrPhone", userid.toString());
                param.put("nickName", nickName.toString());
                param.put("profile_photo", profilePhoto.toString());
                break;
            }
        } catch (IOException e) {
            throw new Http400Exception("IOException",e.getMessage());
        }
    }
}
