package cn.sigo.live.api.service.impl;

import cn.sigo.live.api.filter.Http400Exception;
import cn.sigo.live.api.mapper.Base_TagMapper;
import cn.sigo.live.api.mapper.Post_Linked_TagsMapper;
import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.Post_Linked_Tags;
import cn.sigo.live.api.model.request.PageRequest;
import cn.sigo.live.api.model.request.PostTagsRequest;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.TagCountResponse;
import cn.sigo.live.api.model.response.TagResponse;
import cn.sigo.live.api.service.TagService;
import cn.sigo.live.api.util.ESUpdateHelper;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述:标签管理
 *
 * @author: zhengweilin
 * @date: 2018/12/18
 */
@Service("TagService")
public class TagServiceImpl implements TagService {

    @Autowired
    private Base_TagMapper baseTagMapper;

    @Autowired
    private Post_Linked_TagsMapper postLinkedTagsMapper;

    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * 列表查询
     */
    @Override
    public CommonResponse<TagResponse> getAllTag() {
        CommonResponse<TagResponse> response = new CommonResponse<TagResponse>();
        List<TagResponse> tags = baseTagMapper.getAllTag();
        response.setData(tags);
        response.setSuccess(tags!=null?true:false);

        return response;
    }

    /**
     * 获取所有标签以及标签下帖子数量
     */
    @Override
    public CommonResponse<TagCountResponse> getAllTagAndPostCount() {
        CommonResponse<TagCountResponse> response = new CommonResponse<TagCountResponse>();
        List<TagCountResponse> tags = baseTagMapper.getAllTagAndPostCount();
        response.setData(tags);
        response.setSuccess(tags!=null?true:false);

        return response;
    }

    /**
     * 更新标签排序
     */
    @Override
    public CommonResponse<Null> updateSortById(Integer sortid, boolean isForward) {
        if(sortid==null){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Base_Tag newTag = baseTagMapper.selectBySort(sortid);
        Base_Tag hisTag =isForward? baseTagMapper.selectForwarBySort(sortid):baseTagMapper.selectBehindBySort(sortid);

        if (newTag != null&&hisTag != null) {
            Integer newSortId=newTag.getSort();
            Integer hisSortId=hisTag.getSort();
            newTag.setSort(hisSortId);
            hisTag.setSort(newSortId);
            int isOk=  baseTagMapper.updateByPrimaryKeySelective(newTag);
            isOk= baseTagMapper.updateByPrimaryKeySelective(hisTag);
            response.setSuccess(isOk>0?true:false);
        }

        return response;
    }

    /**
     * 新增
     */
    @Override
    public CommonResponse<Null> insertTag(String name) {
        if(name==null|| "".equals(name)){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Map<String, Object> map = new HashMap<>();
        map.put("tagName",name);
        List<Base_Tag> hisTag=baseTagMapper.selectByName(map);
        if (hisTag!=null&&hisTag.size()>0){
            throw new Http400Exception("dataerror","标签名称重复");
        }

        Integer nextSort=baseTagMapper.selectMaxSort();
        Base_Tag newTag = new Base_Tag();
        newTag.setName(name);
        newTag.setSort(nextSort==null?0:nextSort+1);
        newTag.setState("enabled");
        newTag.setIsDel(false);
        int isOk = baseTagMapper.insertSelective(newTag);
        response.setSuccess(isOk>0?true:false);

        return response;
    }

    /**
     * 更新标签
     */
    @Override
    public CommonResponse<Null> updateTag(Integer tagid, String tagName) {
        if(tagName==null|| "".equals(tagName)){
            throw new Http400Exception("paramerror","参数错误");
        }

        if(tagid==null||tagid<0){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Base_Tag newTag = baseTagMapper.selectByPrimaryKey(tagid);
        newTag.setName(tagName);
        int isOk= baseTagMapper.updateByPrimaryKeySelective(newTag);
        response.setSuccess(isOk>0?true:false);
        List<Integer> postids = postLinkedTagsMapper.selectPostidByTagid(tagid);
        if (postids != null) {
            ////推送es
            ESUpdateHelper.updateByPostId(postids, rabbitTemplate);
        }

        return response;
    }

    /**
     * 删除标签
     */
    @Override
    public CommonResponse<Null> deleteTag(Integer tagid, List<Integer> tagids) {
        if(tagids==null||tagid<0){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Base_Tag newTag = baseTagMapper.selectByPrimaryKey(tagid);
        newTag.setIsDel(true);
        int isOk = baseTagMapper.updateByPrimaryKeySelective(newTag);
        response.setSuccess(isOk>0?true:false);
        List<Integer> postids = postLinkedTagsMapper.selectPostidByTagid(tagid);
        for (Integer postId:postids){
            for(Integer tagId:tagids) {
                Post_Linked_Tags postLinkedTags = new Post_Linked_Tags();
                postLinkedTags.setIsTop(false);
                postLinkedTags.setPostId(postId);
                postLinkedTags.setTagId(tagId);
                postLinkedTags.setAddtime(new Date());
                postLinkedTagsMapper.insertSelective(postLinkedTags);
            }
        }

        if (postids != null) {
            ////推送es
            ESUpdateHelper.updateByPostId(postids, rabbitTemplate);
        }

        return response;
    }

    /**
     * 是否置顶
     */
    @Override
    public CommonResponse<Null> updateIsTop(PostTagsRequest postLinkedTags) {
        if(postLinkedTags.getIsTop()==null||postLinkedTags.getPostIds()==null||postLinkedTags.getTagId()==null){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        boolean isTop= postLinkedTags.getIsTop();
        Integer tagId=postLinkedTags.getTagId();
        int isOk=-1;
        for (Integer postId:postLinkedTags.getPostIds()){
            Map<String, Object> map = new HashMap<>();
            map.put("istop",isTop);
            map.put("postid",postId);
            map.put("tagid",tagId );
            isOk = postLinkedTagsMapper.updateSortById(map);
        }

        response.setSuccess(isOk>0?true:false);

        return response;
    }

    /**
     * 根据排序选择
     */
    @Override
    public Base_Tag selectBySort(Integer sortid) {
        if(sortid==null){
            throw new Http400Exception("paramerror","参数错误");
        }

        return baseTagMapper.selectBySort(sortid);
    }
}
