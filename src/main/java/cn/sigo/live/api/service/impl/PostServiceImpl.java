package cn.sigo.live.api.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.sigo.live.api.filter.Http400Exception;
import cn.sigo.live.api.mapper.PostMapper;
import cn.sigo.live.api.mapper.Post_Linked_TagsMapper;
import cn.sigo.live.api.model.ElasticSearchValue;
import cn.sigo.live.api.model.TagPostByTopDesc;
import cn.sigo.live.api.model.request.PostRequest;
import cn.sigo.live.api.model.request.TagPostTopRequest;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.ItemProperty;
import cn.sigo.live.api.model.response.PageResponse;
import cn.sigo.live.api.model.response.PostListResponse;
import cn.sigo.live.api.service.PostService;
import cn.sigo.live.api.util.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.TermsAggregation;
import org.apache.ibatis.jdbc.Null;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 功能描述:帖子
 *
 * @author: zhengweilin
 * @date: 2018/12/18
 */
@Service("ES_HostService")
public class PostServiceImpl implements PostService {
    @Autowired
    private JestClient jestClient;
    @Autowired
    private PostMapper postMapper;
    @Autowired
    private Post_Linked_TagsMapper postLinkedTagsMapper;
    @Autowired
    private AmqpTemplate rabbitTemplate;
    @Autowired
    private RedisHelper redisHelper;

    /**
     * 列表查询
     */
    @Override
    public PageResponse<PostListResponse, Dictionary<String, String>> searchEntity(PostRequest searchContent) {
        if (searchContent.getPagenum() == null || searchContent.getPagesize() == null) {
            throw new Http400Exception("paramerror", "参数错误");
        }

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        ////ES依条件查询
        if (searchContent.getIdList() != null) {
            boolQueryBuilder.must(QueryBuilders.termsQuery("post_id", searchContent.getIdList()));
        }
        if (!"".equals(searchContent.getTitiledef())) {
            boolQueryBuilder.must(QueryBuilders.wildcardQuery("title.raw", "*" + searchContent.getTitiledef() + "*"));
        }
        if (!"".equals(searchContent.getNumberorphone())) {
            QueryBuilder queryBuilderPhone = QueryBuilders.nestedQuery("publish_user", QueryBuilders.boolQuery().must(QueryBuilders.termQuery("publish_user.phone", "" + searchContent.getNumberorphone() + "")), ScoreMode.None);
            QueryBuilder queryBuilderUserName = QueryBuilders.nestedQuery("publish_user", QueryBuilders.boolQuery().must(QueryBuilders.termQuery("publish_user.username", "" + searchContent.getNumberorphone() + "")), ScoreMode.None);
            boolQueryBuilder.should(queryBuilderPhone);
            boolQueryBuilder.should(queryBuilderUserName);
        }
        if (!"".equals(searchContent.getItemdef())) {
            boolQueryBuilder.must(QueryBuilders.nestedQuery("linked_items", QueryBuilders.boolQuery().must(QueryBuilders.wildcardQuery("linked_items.item_name.raw", "*" + searchContent.getItemdef() + "*")), ScoreMode.None));
        }
        if (!"".equals(searchContent.getPubbegintime()) || !"".equals(searchContent.getPubendtime())) {
            long minPubTime = "".equals(searchContent.getPubbegintime()) ? Long.MIN_VALUE : CommonHelper.dateToTimestamp(searchContent.getPubbegintime() + " 00:00:01");
            long maxPubTime = "".equals(searchContent.getPubendtime()) ? System.currentTimeMillis() : CommonHelper.dateToTimestamp(searchContent.getPubendtime() + " 23:59:59");

            QueryBuilder queryBuilderPubTime = QueryBuilders.rangeQuery("publish_time")
                    .gte(String.valueOf(minPubTime) + "000")
                    .lte(String.valueOf(maxPubTime) + "999")
                    .includeLower(true)
                    .includeUpper(true);
            boolQueryBuilder.must(queryBuilderPubTime);
        }
        if (!"".equals(searchContent.getVisitcountmin()) || !"".equals(searchContent.getVisitcountmax())) {
            String minVisitCount = "".equals(searchContent.getVisitcountmin()) ? String.valueOf(Integer.MIN_VALUE) : searchContent.getVisitcountmin();
            String maxVisitCount = "".equals(searchContent.getVisitcountmax()) ? String.valueOf(Integer.MAX_VALUE) : searchContent.getVisitcountmax();

            QueryBuilder queryBuilderPubTime = QueryBuilders.rangeQuery("count_visit")
                    .gte(minVisitCount)
                    .lte(maxVisitCount)
                    .includeLower(true)
                    .includeUpper(true);
            boolQueryBuilder.must(queryBuilderPubTime);
        }
        if (!"".equals(searchContent.getCommentcountmin()) || !"".equals(searchContent.getCommentcountmax())) {
            String minCommentCount = "".equals(searchContent.getCommentcountmin()) ? String.valueOf(Integer.MIN_VALUE) : searchContent.getCommentcountmin();
            String maxCommentCount = "".equals(searchContent.getCommentcountmax()) ? String.valueOf(Integer.MAX_VALUE) : searchContent.getCommentcountmax();

            QueryBuilder queryBuilderPubTime = QueryBuilders.rangeQuery("count_comment")
                    .gte(12)
                    .lte(maxCommentCount)
                    .includeLower(true)
                    .includeUpper(true);
            boolQueryBuilder.must(queryBuilderPubTime);
        }
        if (!"".equals(searchContent.getState())) {
            boolQueryBuilder.must(QueryBuilders.termsQuery("state", new String[]{"userdeleted", "admindeleted"}));
        }
        if (!"".equals(searchContent.getIsBest())) {
            boolQueryBuilder.must(QueryBuilders.termQuery("is_best", searchContent.getIsBest()));
        }
        if (searchContent.getIsallowcomment()!=null&&!"".equals(searchContent.getIsallowcomment())) {
            boolQueryBuilder.must(QueryBuilders.termQuery("is_allow_comment", searchContent.getIsallowcomment()));
        }
        if (!"".equals(searchContent.getTagname())) {
            boolQueryBuilder.must(QueryBuilders.termQuery("tags.raw", searchContent.getTagname()));
        }

        ////查询参数
        QueryBuilder queryBuilder = boolQueryBuilder;
        searchSourceBuilder.query(queryBuilder);
        searchSourceBuilder.from((searchContent.getPagenum()) * searchContent.getPagesize());
        searchSourceBuilder.size(searchContent.getPagesize());
        if (searchContent.getSortname()!=null&&!"".equals(searchContent.getSortname())){
            searchSourceBuilder.sort(searchContent.getSortname(),searchContent.getIsdesc()=="true"? SortOrder.DESC:SortOrder.ASC);
        }
        else {
            searchSourceBuilder.sort("publish_time", SortOrder.DESC);
        }

        Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex(PostIndexType.INDEX_NAME).addType(PostIndexType.TYPE).build();
        PageResponse<PostListResponse, Dictionary<String, String>> postPages = new PageResponse<PostListResponse, Dictionary<String, String>>();
        try {
            ////查询结果
            JestResult result = jestClient.execute(search);
            if (!result.isSucceeded()) {
                return null;
            }

            ////封装数据
            List<SearchResult.Hit<JSONObject, Void>> hits = ((SearchResult) result).getHits(JSONObject.class);
            postPages.setTotalCount(((SearchResult) result).getTotal());
            List<PostListResponse> listPost = new ArrayList<>();
            for (SearchResult.Hit<JSONObject, Void> hit : hits) {
                PostListResponse postResponse = new PostListResponse();
                JSONObject post = hit.source;
                JSONObject publishUser = post.getJSONObject("publish_user");
                postResponse.setPublishNickname(publishUser == null ? "" : publishUser.getString("nickname"));
                postResponse.setPhoto(publishUser == null ? "" : publishUser.getString("profile_photo") == null ? "" : publishUser.getString("profile_photo"));
                Object cover = post.get("cover");
                postResponse.setCover(cover == null ? "" : cover.toString());
                Integer postid = post.getInteger("post_id");
                postResponse.setPostId(postid == null ? -1 : postid);

                Object title = post.get("title");
                postResponse.setTitle(title == null ? "" : title.toString());
                Object publishTime = post.get("publish_time");
                if (publishTime != null) {
                    BigDecimal bd = new BigDecimal(publishTime.toString());
                    long timespan = Long.parseLong(bd.toPlainString());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    postResponse.setPublishTime(sdf.format(new Date(timespan)));
                }

                Integer countVisit = post.getInteger("count_visit");
                postResponse.setCountVisit(countVisit);
                Integer countComment = post.getInteger("count_comment");
                postResponse.setCountComment(countComment);
                JSONArray linkedItems = post.getJSONArray("linked_items");
                postResponse.setCountItems(linkedItems == null ? 0 : linkedItems.size());

                ////调用Redis查询商品的名称以及图片
                List<ItemProperty> itemList=new ArrayList<>();
                if (linkedItems != null) {
                    for (Object item : linkedItems) {
                        JSONObject itemJson = (JSONObject) item;
                        Integer itemId = itemJson.getInteger("item_id");
                        String itemName = itemJson.getString("item_name");
                        String redisItem = redisHelper.get("b2c_item", StrUtil.toString(itemId));
                        JSONObject jsonItem = (JSONObject) JSONObject.parse(redisItem);
                        String itemImage = jsonItem.getString("image");
                        String redisCategory = redisHelper.get("b2c_item_category", StrUtil.toString(jsonItem.getInteger("category_id")));
                        JSONObject jsonCategory = (JSONObject) JSONObject.parse(redisCategory);
                        String category = jsonCategory.getString("name_en");
                        String url = "https://images.vsigo.cn/Products/" + category + "/" + itemImage;
                        ItemProperty itemProperty=new ItemProperty();
                        itemProperty.setItemId(itemId);
                        itemProperty.setItemName(itemName);
                        itemProperty.setImageUrl(url);
                        itemList.add(itemProperty);
                    }
                }

                postResponse.setItemsAndImage(itemList);
                Object isAllowComment = post.get("is_allow_comment");
                postResponse.setIsAllowComment(title == null ? "" : isAllowComment.toString());
                Object isBest = post.get("is_best");
                postResponse.setIsBest(title == null ? "" : isBest.toString());
                Object state = post.get("state");
                postResponse.setState(state == null ? "" : state.toString());
                listPost.add(postResponse);
            }

            ////聚合tags字段
            ////postPages.setInfo(GetAggsByTag());
            ////设置数据
            postPages.setData(listPost);
        } catch (IOException e) {
            throw new Http400Exception("IOException", e.getMessage());
        }

        return postPages;
    }

    /**
     * 修改状态
     */
    @Override
    public CommonResponse<Null> changeState(List<Integer> postids, boolean isDeleted) {
        if (postids == null) {
            throw new Http400Exception("paramerror", "参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Map<String, Object> map = new HashMap<>();
        map.put("stateName", isDeleted ? PostState.admindeleted : PostState.published);
        map.put("idList", postids);
        int i = postMapper.batchUpdateByPrimaryKey(map);
        response.setSuccess(i > 0 ? true : false);
        if (i > 0) {
            ////推送es
            ESUpdateHelper.updateByPostId(postids, rabbitTemplate);
        }

        return response;
    }

    /**
     * 是否可评论
     */
    @Override
    public CommonResponse<Null> changeAllowComment(List<Integer> postids, boolean isAllow) {
        if (postids == null) {
            throw new Http400Exception("paramerror", "参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Map<String, Object> map = new HashMap<>();
        map.put("allowComment", isAllow ? 1 : 0);
        map.put("idList", postids);
        int i = postMapper.batchUpdateByPrimaryKey(map);
        response.setSuccess(i > 0 ? true : false);
        if (i > 0) {
            for (Integer item : postids) {
                ElasticSearchValue es = new ElasticSearchValue();
                es.setEsIdValue(item.toString());
                rabbitTemplate.convertAndSend("exchange.sigo.b2c.elasticsearch", "index_live_post", JSON.toJSONString(es));
            }
        }

        return response;
    }

    /**
     * 是否精华
     */
    @Override
    public CommonResponse<Null> changeBest(List<Integer> postids, boolean isBest) {
        if (postids == null) {
            throw new Http400Exception("paramerror", "参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Map<String, Object> map = new HashMap<>();
        map.put("isBest", isBest ? 1 : 0);
        map.put("idList", postids);
        int i = postMapper.batchUpdateByPrimaryKey(map);
        response.setSuccess(i > 0 ? true : false);
        if (i > 0) {
            for (Integer item : postids) {
                ElasticSearchValue es = new ElasticSearchValue();
                es.setEsIdValue(item.toString());
                rabbitTemplate.convertAndSend("exchange.sigo.b2c.elasticsearch", "index_live_post", JSON.toJSONString(es));
            }
        }

        return response;
    }

    /**
     * 根据标签查询帖子(置顶)数据
     */
    @Override
    public PageResponse<PostListResponse, Dictionary<String, String>> getTagPostTop(TagPostTopRequest tagPostTopRequest) {
        if (tagPostTopRequest.getTagId() == null || tagPostTopRequest.getPagenum() == null || tagPostTopRequest.getPagesize() == null) {
            throw new Http400Exception("paramerror", "参数错误");
        }

        PageResponse<PostListResponse, Dictionary<String, String>> response = new PageResponse<PostListResponse, Dictionary<String, String>>();
        Map<String, Object> param = new HashMap<>();
        if (tagPostTopRequest.getPubbegintime() != null && !"".equals(tagPostTopRequest.getPubbegintime())) {
            tagPostTopRequest.setPubbegintime(tagPostTopRequest.getPubbegintime() + " 00:00:01");
        }
        if (tagPostTopRequest.getPubendtime() != null && !"".equals(tagPostTopRequest.getPubendtime())) {
            tagPostTopRequest.setPubendtime(tagPostTopRequest.getPubendtime() + " 23:59:59");
        }

        ////查询条件封装
        param.put("tagid", tagPostTopRequest.getTagId());
        param.put("posttitle", tagPostTopRequest.getPostTitle());
        param.put("publishusername", tagPostTopRequest.getPublishUserName());
        param.put("pubbegintime", tagPostTopRequest.getPubbegintime());
        param.put("pubendtime", tagPostTopRequest.getPubendtime());
        param.put("isbest", tagPostTopRequest.getIsbest());
        param.put("delstate", tagPostTopRequest.getDelState());

        PageHelper.startPage(tagPostTopRequest.getPagenum() + 1, tagPostTopRequest.getPagesize());
        List<TagPostByTopDesc> list = postLinkedTagsMapper.selectTagPostByTopDesc(param);

        List<Integer> postids = new ArrayList<>();
        list.forEach((x) -> postids.add(x.getPostId()));
        PostRequest searchContent = new PostRequest();
        searchContent.setPagenum(0);
        searchContent.setPagesize(100);
        searchContent.setIdList(postids);
        searchContent.setIsallowcomment(tagPostTopRequest.getIsallowcomment());
        searchContent.setSortname(tagPostTopRequest.getSortname());
        searchContent.setIsdesc(tagPostTopRequest.getIsdesc());

        PageResponse<PostListResponse, Dictionary<String, String>> result = searchEntity(searchContent);
        List<PostListResponse> resultData = result.getData();
        resultData.forEach(x ->
                x.setIsTop(list.stream().filter(s -> s.getPostId().equals(x.getPostId())).findFirst().get().getIsTop() ? 1 : 0)
        );

        resultData.forEach(x ->
                x.setTagId(tagPostTopRequest.getTagId())
        );

        resultData = resultData.stream().sorted(Comparator.comparing(PostListResponse::getIsTop).reversed())
                .collect(Collectors.toList());

        Dictionary<String, String> total = new Hashtable<String, String>();
        Integer totalTop = postLinkedTagsMapper.selectTotalTop(tagPostTopRequest.getTagId());
        total.put("tagTopCount", totalTop.toString());

        PageInfo pageInfo = new PageInfo(list);
        response.setData(resultData);
        response.setTotalCount(pageInfo.getTotal());
        response.setInfo(total);
        return response;
    }

    /**
     * 聚合tag
     */
    private Dictionary<String, String> getAggsByTag() {
        Dictionary<String, String> tagAggsDic = new Hashtable<String, String>();
        try {
            SearchSourceBuilder searchSourceBuilderAgg = new SearchSourceBuilder();
            AggregationBuilder teamAgg = AggregationBuilders.terms("aggs_count").field("tags.raw");
            searchSourceBuilderAgg.aggregation(teamAgg);
            Search searchAgg = new Search.Builder(searchSourceBuilderAgg.toString())
                    .addIndex(PostIndexType.INDEX_NAME).addType(PostIndexType.TYPE).build();
            JestResult resultAgg = jestClient.execute(searchAgg);
            List<TermsAggregation.Entry> aggregations = ((SearchResult) resultAgg).getAggregations().getTermsAggregation("aggs_count").getBuckets();
            for (TermsAggregation.Entry entry : aggregations) {
                String tagName = entry.getKeyAsString();
                String count = entry.getCount().toString();
                tagAggsDic.put(tagName, count);
            }
        } catch (IOException e) {
            throw new Http400Exception("IOException", e.getMessage());
        }

        return tagAggsDic;
    }
}
