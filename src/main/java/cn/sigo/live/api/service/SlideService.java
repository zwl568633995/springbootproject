package cn.sigo.live.api.service;

import cn.sigo.live.api.model.Best_Slide;
import cn.sigo.live.api.model.response.BestSlideResponse;
import cn.sigo.live.api.model.response.CommonResponse;

import javax.validation.constraints.Null;
import java.util.List;

/**
 *  轮播管理
 */
public interface SlideService {
    /**
     *  新增
     */
    CommonResponse<Null> insert(Best_Slide record);

    /**
     *  新增
     */
    CommonResponse<Null> insertBatch(List<Best_Slide> records);

    /**
     *  删除
     */
    CommonResponse<Null> deleteByPrimaryKey(Integer id);

    /**
     *  获取所有数据
     */
    CommonResponse<BestSlideResponse> getAll();

    /**
     *  更新顺序
     */
    CommonResponse<Null> updateSortById(Integer sortid, boolean isForward);
}
