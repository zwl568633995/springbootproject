package cn.sigo.live.api.service;

import cn.sigo.live.api.model.request.PostRequest;
import cn.sigo.live.api.model.request.TagPostTopRequest;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.PageResponse;
import cn.sigo.live.api.model.response.PostListResponse;
import org.apache.ibatis.jdbc.Null;

import java.util.Dictionary;
import java.util.List;

/**
 *  帖子管理
 */
public interface PostService {
    /**
     *  列表
     */
    PageResponse<PostListResponse, Dictionary<String,String>> searchEntity(PostRequest searchContent);

    /**
     *  修改状态
     */
    CommonResponse<Null> changeState(List<Integer> postids, boolean isDeleted);

    /**
     *  是否可评论
     */
    CommonResponse<Null> changeAllowComment(List<Integer> postids, boolean isAllow);

    /**
     *  是否精华
     */
    CommonResponse<Null> changeBest(List<Integer> postids, boolean isBest);

    /**
     *  根据标签查询帖子根据是否置顶牌组
     */
    PageResponse<PostListResponse, Dictionary<String,String>> getTagPostTop(TagPostTopRequest tagPostTopRequest);
}
