package cn.sigo.live.api.service.impl;

import cn.sigo.live.api.filter.Http400Exception;
import cn.sigo.live.api.mapper.Best_SlideMapper;
import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.Best_Slide;
import cn.sigo.live.api.model.response.*;
import cn.sigo.live.api.service.SlideService;
import cn.sigo.live.api.util.CommonHelper;
import cn.sigo.live.api.util.PostIndexType;
import com.alibaba.fastjson.JSONObject;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 功能描述:轮播管理
 *
 * @author: zhengweilin
 * @date: 2018/01-07
 */
@Service("SlideService")
public class SlideServiceImpl implements SlideService {
    @Autowired
    private Best_SlideMapper bestSlideMapper;
    @Autowired
    private JestClient jestClient;
    /**
     * 新增轮播数据
     */
    @Override
    public CommonResponse<Null> insert(Best_Slide record) {
        return null;
    }

    /**
     * 批量新增轮播数据
     */
    @Override
    public CommonResponse<Null> insertBatch(List<Best_Slide> records) {
        CommonResponse<Null> response = new CommonResponse<Null>();
        int isOk=-1;
        for (Best_Slide bestSlide:records){
            Integer nextSort=bestSlideMapper.selectMaxSort();
            bestSlide.setSort(nextSort==null?0:nextSort+1);
            bestSlide.setAddtime(new Date());
            bestSlide.setState("enabled");
            isOk=bestSlideMapper.insertSelective(bestSlide);
        }

        response.setSuccess(isOk>0?true:false);

        return response;
    }

    /**
     * 删除轮播
     */
    @Override
    public CommonResponse<Null> deleteByPrimaryKey(Integer id) {
        CommonResponse<Null> response = new CommonResponse<Null>();
        Best_Slide slide= bestSlideMapper.selectByPrimaryKey(id);
        slide.setState("disabled");
        int isOk=bestSlideMapper.updateByPrimaryKeySelective(slide);
        response.setSuccess(isOk>0?true:false);

        return response;
    }

    /**
     * 获取所有轮播
     */
    @Override
    public CommonResponse<BestSlideResponse> getAll() {
        CommonResponse<BestSlideResponse> response = new CommonResponse<BestSlideResponse>();
        List<BestSlideResponse> slides = bestSlideMapper.getAll();
        slides.forEach(x->x.setAddtimestr(CommonHelper.stampToDate(x.getAddtime().getTime())));
        List<String> userids = new ArrayList<String>();
        for (BestSlideResponse slide : slides) {
            userids.add(slide.getPostid().toString());
        }

        getPostInfoByEs(userids,slides);
        response.setData(slides);
        response.setSuccess(slides!=null?true:false);

        return response;
    }

    /**
     * 更新轮播排序
     */
    @Override
    public CommonResponse<Null> updateSortById(Integer sortid, boolean isForward) {
        if(sortid==null){
            throw new Http400Exception("paramerror","参数错误");
        }

        CommonResponse<Null> response = new CommonResponse<Null>();
        Best_Slide newSlide = bestSlideMapper.selectBySort(sortid);
        Best_Slide hisSlide =isForward? bestSlideMapper.selectForwarBySort(sortid):bestSlideMapper.selectBehindBySort(sortid);

        if (newSlide != null&&hisSlide != null) {
            Integer newSortId=newSlide.getSort();
            Integer hisSortId=hisSlide.getSort();
            newSlide.setSort(hisSortId);
            hisSlide.setSort(newSortId);
            int isOk=  bestSlideMapper.updateByPrimaryKeySelective(newSlide);
            isOk= bestSlideMapper.updateByPrimaryKeySelective(hisSlide);
            response.setSuccess(isOk>0?true:false);
        }

        return response;
    }

    /**
     * Es获取NickName
     */
    private void getPostInfoByEs(List<String> postids, List<BestSlideResponse> list) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        QueryBuilder queryBuilderPhone = QueryBuilders.termsQuery("post_id", postids);

        boolQueryBuilder.must(queryBuilderPhone);
        ////查询参数
        QueryBuilder queryBuilder = boolQueryBuilder;
        searchSourceBuilder.query(queryBuilder);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex(PostIndexType.INDEX_NAME).addType(PostIndexType.TYPE).build();
        try {
            ////查询结果
            JestResult result = jestClient.execute(search);
            if (!result.isSucceeded()) {
                return;
            }

            List<SearchResult.Hit<JSONObject, Void>> hits = ((SearchResult) result).getHits(JSONObject.class);
            PageResponse<PostListResponse, Null> postPages = new PageResponse<PostListResponse, Null>();
            postPages.setTotalCount(hits.size());
            List<PostListResponse> listPost = new ArrayList<>();
            for (SearchResult.Hit<JSONObject, Void> hit : hits) {
                PostListResponse postResponse = new PostListResponse();
                JSONObject post = hit.source;
                Integer postId = post.getInteger("post_id");
                String titile=post.getString("title");
                String cover=post.getString("cover");
                String heading=post.getString("heading");

                for (BestSlideResponse coment : list) {
                    if (coment.getPostid().equals(postId)) {
                        coment.setTitle(titile);
                        coment.setCover(cover);
                        coment.setHeading(heading);
                    }
                }
            }
        } catch (IOException e) {
            throw new Http400Exception("IOException",e.getMessage());
        }
    }
}
