package cn.sigo.live.api.service;

import cn.sigo.live.api.model.request.CommentRequest;
import cn.sigo.live.api.model.response.CommentListResponse;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.PageResponse;

import javax.validation.constraints.Null;
import java.util.List;

/**
 *  评论管理
 */
public interface CommentService {
    /**
     *  列表
     */
    PageResponse<CommentListResponse, Null> query(CommentRequest postRequest);

    /**
     *  修改状态
     */
    CommonResponse<org.apache.ibatis.jdbc.Null> changeState(List<Integer> postids, boolean isEnabled);
}
