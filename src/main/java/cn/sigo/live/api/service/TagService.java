package cn.sigo.live.api.service;

import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.Post_Linked_Tags;
import cn.sigo.live.api.model.request.PostTagsRequest;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.TagCountResponse;
import cn.sigo.live.api.model.response.TagResponse;

import javax.validation.constraints.Null;
import java.util.List;

/**
 *  标签管理
 */
public interface TagService {
    /**
     * 获取所有标签
     */
    CommonResponse<TagResponse> getAllTag();

    /**
     * 获取所有标签
     */
    CommonResponse<TagCountResponse> getAllTagAndPostCount();

    /**
     * 更新标签
     */
    CommonResponse<Null> updateSortById(Integer id, boolean isForward);

    /**
     * 新增标签
     */
    CommonResponse<Null> insertTag(String name) ;

    /**
     * 根据sort查询标签数据
     */
    Base_Tag selectBySort(Integer sortid);

    /**
     * 根据tagid更新数据
     */
    CommonResponse<Null> updateTag(Integer tagid, String tagName);

    /**
     * 根据tagid更新数据
     */
    CommonResponse<Null> deleteTag(Integer tagid, List<Integer> tagids);

    /**
     * 根据tagid,postid更新是否置顶
     */
    CommonResponse<Null> updateIsTop(PostTagsRequest postLinkedTags);
}
