package cn.sigo.live.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * spring boot启动类
 * @author zhengweilin
 * @date 2018/12/31
 */
@SpringBootApplication
@MapperScan("cn.sigo.live.api.mapper") // 将项目中对应的mapper类的路径加进来就可以了
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
