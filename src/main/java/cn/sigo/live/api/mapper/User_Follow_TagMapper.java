package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.User_Follow_Tag;

public interface User_Follow_TagMapper {
    int insert(User_Follow_Tag record);

    int insertSelective(User_Follow_Tag record);
}