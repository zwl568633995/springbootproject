package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post_Comment;
import cn.sigo.live.api.model.response.CommentListResponse;

import java.util.List;
import java.util.Map;

public interface Post_CommentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Post_Comment record);

    int insertSelective(Post_Comment record);

    Post_Comment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Post_Comment record);

    int updateByPrimaryKey(Post_Comment record);

    List<CommentListResponse> query(Map<String,Object> param);

    int batchUpdateByPrimaryKey(Map<String,Object> map);
}