package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.User_Follow_User;

public interface User_Follow_UserMapper {
    int insert(User_Follow_User record);

    int insertSelective(User_Follow_User record);
}