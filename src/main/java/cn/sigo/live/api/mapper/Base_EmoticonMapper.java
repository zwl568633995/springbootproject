package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Base_Emoticon;

public interface Base_EmoticonMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Base_Emoticon record);

    int insertSelective(Base_Emoticon record);

    Base_Emoticon selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Base_Emoticon record);

    int updateByPrimaryKey(Base_Emoticon record);
}