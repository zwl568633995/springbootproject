package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Base_Media;

public interface Base_MediaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Base_Media record);

    int insertSelective(Base_Media record);
}