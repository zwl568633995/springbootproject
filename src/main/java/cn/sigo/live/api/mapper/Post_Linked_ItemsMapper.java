package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post_Linked_Items;

public interface Post_Linked_ItemsMapper {
    int insert(Post_Linked_Items record);

    int insertSelective(Post_Linked_Items record);
}