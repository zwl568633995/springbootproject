package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.response.TagCountResponse;
import cn.sigo.live.api.model.response.TagResponse;

import java.util.List;
import java.util.Map;

/**
 *  标签管理
 */
public interface Base_TagMapper {
    /**
     *  根据主键删除
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *  插入
     */
    int insert(Base_Tag record);

    /**
     *  插入(非空判断)
     */
    int insertSelective(Base_Tag record);

    /**
     *  查询
     */
    Base_Tag selectByPrimaryKey(Integer id);

    /**
     *  更新(非空)
     */
    int updateByPrimaryKeySelective(Base_Tag record);

    /**
     *  更新
     */
    int updateByPrimaryKey(Base_Tag record);

    /**
     *  get所有
     */
    List<TagResponse> getAllTag();

    /**
     *  根据标签id更新顺序
     */
    int updateSortById(Map<String,Object> map);

    /**
     *  根据标签sort查询数据
     */
    Base_Tag selectBySort(Integer sortid);

    /**
     *  根据标签sort查询前一条数据
     */
    Base_Tag selectForwarBySort(Integer sortid);

    /**
     *  根据标签sort查询后一条数据
     */
    Base_Tag selectBehindBySort(Integer sortid);

    /**
     *  查询标签最大排序
     */
    Integer selectMaxSort();

    /**
     *  获取所有标签以及帖子的数量
     */
    List<TagCountResponse> getAllTagAndPostCount();

    /**
     *  根据标签名称查询
     */
    List<Base_Tag> selectByName(Map<String,Object> map);
}