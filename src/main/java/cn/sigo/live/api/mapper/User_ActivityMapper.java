package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.User_Activity;

public interface User_ActivityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User_Activity record);

    int insertSelective(User_Activity record);

    User_Activity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User_Activity record);

    int updateByPrimaryKey(User_Activity record);
}