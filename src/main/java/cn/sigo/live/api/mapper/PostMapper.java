package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post;
import cn.sigo.live.api.model.response.PostListResponse;

import java.util.List;
import java.util.Map;

/**
 *  帖子管理
 */
public interface PostMapper {
    /**
     *  根据主键删除帖子
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *  插入
     */
    int insert(Post record);

    /**
     *  插入(非空判断)
     */
    int insertSelective(Post record);

    /**
     *  根据主键查询
     */
    Post selectByPrimaryKey(Integer id);

    /**
     *  更新(非空判断)
     */
    int updateByPrimaryKeySelective(Post record);

    /**
     *  更新
     */
    int updateByPrimaryKeyWithBLOBs(Post record);

    /**
     *  根据主键更新
     */
    int updateByPrimaryKey(Post record);

    /**
     *  查询
     */
    List<PostListResponse> query(Map<String,Object> param);

    /**
     *  根据主键批量更新
     */
    int batchUpdateByPrimaryKey(Map<String,Object> map);
}