package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post_Linked_Tags;
import cn.sigo.live.api.model.TagPostByTopDesc;

import java.util.List;
import java.util.Map;

public interface Post_Linked_TagsMapper {
    int insert(Post_Linked_Tags record);

    int insertSelective(Post_Linked_Tags record);

    List<Integer> selectPostidByTagid(Integer tagid);

    List<TagPostByTopDesc> selectTagPostByTopDesc(Map<String,Object> map);

    Integer selectTotalTop(Integer tagid);

    int  updateSortById(Map<String,Object> map);
}