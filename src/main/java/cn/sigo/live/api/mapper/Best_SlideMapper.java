package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.Best_Slide;
import cn.sigo.live.api.model.response.BestSlideResponse;

import java.util.List;

/**
 *  轮播页管理
 */
public interface Best_SlideMapper {
    /**
     *  删除
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *  插入数据
     */
    int insert(Best_Slide record);

    /**
     *  插入数据(非空判断)
     */
    int insertSelective(Best_Slide record);

    /**
     *  根据主键查询数据
     */
    Best_Slide selectByPrimaryKey(Integer id);

    /**
     *  根据主键更新数据(非空判断)
     */
    int updateByPrimaryKeySelective(Best_Slide record);

    /**
     *  根据主键更新数据
     */
    int updateByPrimaryKey(Best_Slide record);

    /**
     *  查询最大sort
     */
    Integer selectMaxSort();

    /**
     *  获取所有数据
     */
    List<BestSlideResponse> getAll();

    /**
     *  根据标签sort查询前一条数据
     */
    Best_Slide selectForwarBySort(Integer sortid);

    /**
     *  根据标签sort查询后一条数据
     */
    Best_Slide selectBehindBySort(Integer sortid);

    /**
     *  根据标签sort查询数据
     */
    Best_Slide selectBySort(Integer sortid);
}