package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post_Like;

public interface Post_LikeMapper {
    int insert(Post_Like record);

    int insertSelective(Post_Like record);
}