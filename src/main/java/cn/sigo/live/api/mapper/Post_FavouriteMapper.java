package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post_Favourite;

public interface Post_FavouriteMapper {
    int insert(Post_Favourite record);

    int insertSelective(Post_Favourite record);
}