package cn.sigo.live.api.mapper;

import cn.sigo.live.api.model.Post_Comment_Like;

public interface Post_Comment_LikeMapper {
    int insert(Post_Comment_Like record);

    int insertSelective(Post_Comment_Like record);
}