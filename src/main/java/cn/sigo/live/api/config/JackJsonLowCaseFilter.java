package cn.sigo.live.api.config;

import com.alibaba.fastjson.serializer.NameFilter;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

/**
 * 功能描述:JackJSON自定义
 */
public class JackJsonLowCaseFilter implements NameFilter {
    public JackJsonLowCaseFilter() {
    }

    @Override
    public String process(Object source, String name, Object value) {
        if (name != null && name.length() != 0) {
           return  name.toLowerCase();
        } else {
            return name;
        }
    }
}
