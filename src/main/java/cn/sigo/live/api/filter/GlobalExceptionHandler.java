package cn.sigo.live.api.filter;

import cn.hutool.core.convert.Convert;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述: 全局异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(value = Http400Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public HttpExceptionResponse handlerException(HttpServletRequest request, Exception ex)
    {
        if(ex instanceof Http400Exception)
        {
            Http400Exception http400Exception=(Http400Exception)ex;
            HttpExceptionResponse response= HttpExceptionResponse.create(http400Exception.getErrorCode(), http400Exception.getMessage());
            return response;
        }
        else {
            return HttpExceptionResponse.create(Convert.toStr(HttpStatus.INTERNAL_SERVER_ERROR.value()), ex.getMessage());
        }
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public HttpExceptionResponse requestHandlerException(HttpServletRequest request, Exception ex)
    {
        if(ex instanceof HttpMessageNotReadableException)
        {
            HttpExceptionResponse response= HttpExceptionResponse.create("paramerror", "参数错误");
            return response;
        }
        else {
            return HttpExceptionResponse.create(Convert.toStr(HttpStatus.INTERNAL_SERVER_ERROR.value()), ex.getMessage());
        }
    }
}
