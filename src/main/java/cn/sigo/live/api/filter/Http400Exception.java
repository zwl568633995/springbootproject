package cn.sigo.live.api.filter;

/**
 * 功能描述: 自定义400消息异常
 */
public class Http400Exception extends RuntimeException {
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    private String errorCode;

    public Http400Exception(String errorCode, String errorMsg) {
        super(errorMsg);
        this.errorCode = errorCode;
    }
}
