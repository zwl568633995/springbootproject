package cn.sigo.live.api.filter;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 功能描述: 自定义异常消息返回
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class HttpExceptionResponse {
    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    private String error_code;
    private String error_msg;

    public HttpExceptionResponse(String errorCode) {
        this.error_code = errorCode;
    }
    public HttpExceptionResponse(String errorCode, String errorMsg) {
        this.error_code = errorCode;
        this.error_msg = errorMsg;
    }

    public static HttpExceptionResponse create(String errorCode, String errorMsg) {
        return new HttpExceptionResponse(errorCode, errorMsg);
    }
}