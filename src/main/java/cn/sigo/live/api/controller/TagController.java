package cn.sigo.live.api.controller;

import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.Post_Linked_Tags;
import cn.sigo.live.api.model.request.PostTagsRequest;
import cn.sigo.live.api.model.request.TagRequest;
import cn.sigo.live.api.model.response.*;
import cn.sigo.live.api.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.List;

/**
 * 功能描述:标签管理
 *
 * @author: zhengweilin
 * @date: 2018/12/26
 */
@RestController
@RequestMapping("/tag")
public class TagController {
    @Autowired
    private TagService tagService;

    /**
     * 查询列表
     */
    @PostMapping(value = "/getall", produces = {"application/json;charset=utf-8"})
    public CommonResponse<TagResponse> list() {
        return tagService.getAllTag();
    }

    /**
     * 设置前后顺序
     */
    @PostMapping(value = "/setsort/{sortid}/{isforward}", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> setsort(@PathVariable Integer sortid,@PathVariable boolean isforward) {
        return tagService.updateSortById(sortid,isforward);
    }

    /**
     * 新增数据
     */
    @PostMapping(value = "/insert", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> insertTag(@RequestBody Base_Tag tag) {
        return tagService.insertTag(tag.getName());
    }

    /**
     * 更新标签
     */
    @PostMapping(value = "/update", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> updateTag(@RequestBody TagRequest tag) {
        return tagService.updateTag(tag.getTagId(),tag.getName());
    }

    /**
     * 删除标签
     */
    @PostMapping(value = "/delete", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> deleteTag(@RequestBody TagRequest tag) {
        return tagService.deleteTag(tag.getTagId(),tag.getUpdateTagIds());
    }

    /**
     * 获取所有标签下的帖子数量
     */
    @PostMapping(value = "/gettagpostcount", produces = {"application/json;charset=utf-8"})
    public CommonResponse<TagCountResponse> tagcountlist() {
        return tagService.getAllTagAndPostCount();
    }

    /**
     * 更新是否置顶
     */
    @PostMapping(value = "/settop", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> updateIsTop(@RequestBody PostTagsRequest postLinkedTags) {
        return tagService.updateIsTop(postLinkedTags);
    }
}
