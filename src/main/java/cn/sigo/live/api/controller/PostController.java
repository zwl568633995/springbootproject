package cn.sigo.live.api.controller;

import cn.sigo.live.api.model.request.BatchRequest;
import cn.sigo.live.api.model.request.PostRequest;
import cn.sigo.live.api.model.request.TagPostTopRequest;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.PageResponse;
import cn.sigo.live.api.model.response.PostListResponse;
import cn.sigo.live.api.service.PostService;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Dictionary;

/**
 * 功能描述:内容管理查询帖子
 *
 * @author: zhengweilin
 * @date: 2018/12/18
 */
@RestController
@RequestMapping("/post")
public class PostController {
    @Autowired
    private PostService esHostService;

    /**
     * 查询列表
     */
    @PostMapping(value = "/list", produces = {"application/json;charset=utf-8"})
    public PageResponse<PostListResponse, Dictionary<String,String>> list(@RequestBody PostRequest searchContent) {
        return esHostService.searchEntity(searchContent);
    }

    /**
     * 删除
     */
    @PostMapping(value = "/delete", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> delete(@RequestBody BatchRequest<Integer> request) {
        return esHostService.changeState(request.getCollection(),true);
    }

    /**
     * 撤销删除
     */
    @PostMapping(value = "/repealdelete", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> repealdelete(@RequestBody BatchRequest<Integer> request) {
        return esHostService.changeState(request.getCollection(),false);
    }

    /**
     * 设置评论切换
     */
    @PostMapping(value = "/setcommentswitch/{isallow}", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> setcommentswitch(@RequestBody BatchRequest<Integer> request,@PathVariable boolean isallow) {
       return esHostService.changeAllowComment(request.getCollection(),isallow);
    }

    /**
     * 设置精华切换
     */
    @PostMapping(value = "/setbestswitch/{isbest}", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> setbestswitch(@RequestBody BatchRequest<Integer> request,@PathVariable boolean isbest) {
        return esHostService.changeBest(request.getCollection(),isbest);
    }

    /**
     * 设置精华切换
     */
    @PostMapping(value = "/gettagposttop", produces = {"application/json;charset=utf-8"})
    public PageResponse<PostListResponse, Dictionary<String,String>> gettagposttop(@RequestBody TagPostTopRequest tagPostTopRequest) {
        return esHostService.getTagPostTop(tagPostTopRequest);
    }
}
