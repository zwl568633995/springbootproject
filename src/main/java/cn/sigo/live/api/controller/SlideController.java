package cn.sigo.live.api.controller;


import cn.sigo.live.api.model.Base_Tag;
import cn.sigo.live.api.model.Best_Slide;
import cn.sigo.live.api.model.response.BestSlideResponse;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.TagResponse;
import cn.sigo.live.api.service.SlideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.List;

/**
 * 功能描述:图片轮播
 *
 * @author: zhengweilin
 * @date: 2018/12/18
 */
@RestController
@RequestMapping("/slide")
public class SlideController {

    @Autowired
    private SlideService slideService;

    /**
     * 新增数据
     */
    @PostMapping(value = "/insertbatch", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> insertbatch(@RequestBody List<Best_Slide> slides) {
        return slideService.insertBatch(slides);
    }

    /**
     * 查询列表
     */
    @PostMapping(value = "/getall", produces = {"application/json;charset=utf-8"})
    public CommonResponse<BestSlideResponse> list() {
        return slideService.getAll();
    }

    /**
     * 设置前后顺序
     */
    @PostMapping(value = "/setsort/{sortid}/{isforward}", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> setsort(@PathVariable Integer sortid, @PathVariable boolean isforward) {
        return slideService.updateSortById(sortid,isforward);
    }

    /**
     * 删除轮播
     */
    @PostMapping(value = "/delete/{slideid}", produces = {"application/json;charset=utf-8"})
    public CommonResponse<Null> delete(@PathVariable Integer slideid) {
        return slideService.deleteByPrimaryKey(slideid);
    }
}
