package cn.sigo.live.api.controller;

import cn.sigo.live.api.model.request.BatchRequest;
import cn.sigo.live.api.model.request.CommentRequest;
import cn.sigo.live.api.model.response.CommentListResponse;
import cn.sigo.live.api.model.response.CommonResponse;
import cn.sigo.live.api.model.response.PageResponse;
import cn.sigo.live.api.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Null;

/**
 * 功能描述:评论管理
 *
 * @author: zhengweilin
 * @date: 2018/12/18
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    /**
     * 查询列表
     */
    @PostMapping(value = "/list", produces = {"application/json;charset=utf-8"})
    public PageResponse<CommentListResponse, Null> list(@RequestBody CommentRequest searchContent) {
        return commentService.query(searchContent);
    }

    /**
     * 删除
     */
    @PostMapping(value = "/delete", produces = {"application/json;charset=utf-8"})
    public CommonResponse<org.apache.ibatis.jdbc.Null> delete(@RequestBody BatchRequest<Integer> request) {
        return commentService.changeState(request.getCollection(),false);
    }

    /**
     * 撤销删除
     */
    @PostMapping(value = "/repealdelete", produces = {"application/json;charset=utf-8"})
    public CommonResponse<org.apache.ibatis.jdbc.Null> repealdelete(@RequestBody BatchRequest<Integer> request) {
        return commentService.changeState(request.getCollection(),true);
    }
}
